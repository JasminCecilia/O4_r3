package harjoitus;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;

public class PeliLoppu extends JFrame {

	float oikein, yht;
	int ID; // ID 1 = varitSV, 2 = elaimetSV, 3 = varitEN
	JLabel otsikko, info, lkm, prosentti;
	JButton pelaaUudelleen, etusivu;

	public PeliLoppu(int O, int Y, int id) {
		oikein = O;
		yht = Y;
		ID = id;
		rakenna();
	}

	private void rakenna() {

		Color etusivuTausta = new Color(10, 123, 245);
		Font f1 = new Font(Font.SERIF, Font.BOLD, 60);
		Font f2 = new Font(Font.SERIF, Font.PLAIN, 20);

		setSize(800, 1000);
		setVisible(true);

		JPanel mainPanel = new JPanel();
		JPanel otsake = new JPanel();
		JPanel laskut = new JPanel();
		// JPanel pelaa = new JPanel();
		JPanel tyhja = new JPanel();

		otsake.setLayout(new GridBagLayout());
		GridBagConstraints c = new GridBagConstraints();

		laskut.setLayout(new BoxLayout(laskut, BoxLayout.Y_AXIS));
		laskut.setAlignmentX(CENTER_ALIGNMENT);

		mainPanel.setLayout(new BorderLayout());
		// pelaa.setLayout(new BoxLayout(laskut, BoxLayout.Y_AXIS));
		tyhja.setLayout(new BoxLayout(tyhja, BoxLayout.Y_AXIS));

		JButton etusivu = new JButton("Palaa etusivulle");
		etusivu.setBackground(etusivuTausta);
		etusivu.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				O4harjoitus etusivu = new O4harjoitus();
				dispose();
				Dimension koko = Toolkit.getDefaultToolkit().getScreenSize();
				etusivu.setSize(koko.width, koko.height);
				etusivu.setVisible(true);
			}
		});

		JButton pelaaUudelleen = new JButton("Pelaa uudelleen");
		pelaaUudelleen.setAlignmentX(JComponent.CENTER_ALIGNMENT);
		pelaaUudelleen.setBackground(Color.yellow);
		// pelaaUudelleen.setMinimumSize(getMinimumSize());

		JLabel otsikko = new JLabel("Peli on paattynyt", SwingConstants.CENTER);
		// otsikko.setAlignmentX(JComponent.CENTER_ALIGNMENT);

		JLabel info = new JLabel("Sait oikeita vastauksia");
		JLabel lkm = new JLabel(Math.round(oikein) + "/" + Math.round(yht));
		JLabel prosentti = new JLabel("Vastasit oikein " + Math.round(oikein / yht * 100) + " %");

		info.setAlignmentX(JComponent.CENTER_ALIGNMENT);
		lkm.setAlignmentX(JComponent.CENTER_ALIGNMENT);
		prosentti.setAlignmentX(JComponent.CENTER_ALIGNMENT);

		info.setFont(f2);
		lkm.setFont(f2);
		prosentti.setFont(f2);

		laskut.add(info);
		laskut.add(lkm);
		laskut.add(prosentti);
		laskut.add(pelaaUudelleen);

		otsikko.setFont(f1);

		c.anchor = GridBagConstraints.WEST;
		otsake.add(etusivu, c);
		c.anchor = GridBagConstraints.CENTER;
		otsake.add(otsikko, c);

		mainPanel.add(otsake, BorderLayout.NORTH);
		mainPanel.add(laskut, BorderLayout.CENTER);

		add(mainPanel);
		Dimension koko = Toolkit.getDefaultToolkit().getScreenSize();
		setSize(koko.width, koko.height);
		setVisible(true);

		pelaaUudelleen.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				dispose();
				Dimension koko = Toolkit.getDefaultToolkit().getScreenSize();
				switch (ID) {
				case 1:
					PeliVaritSV varitSV = new PeliVaritSV();
					varitSV.setSize(koko.width, koko.height);
					varitSV.setVisible(true);
				case 2:
					PeliElaimetSV elaimetSV = new PeliElaimetSV();
					elaimetSV.setSize(koko.width, koko.height);
					elaimetSV.setVisible(true);
				case 3:
					PeliVaritEN varitEN = new PeliVaritEN();
					varitEN.setSize(koko.width, koko.height);
					varitEN.setVisible(true);
				}
			}
		});
	}

}
