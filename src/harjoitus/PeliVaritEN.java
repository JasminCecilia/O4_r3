package harjoitus;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.util.Arrays;
import java.util.List;
import java.util.Random;

import javax.swing.*;
import javax.swing.border.LineBorder;
import java.awt.BorderLayout;

public class PeliVaritEN extends JFrame {

	static Random generator = new Random();

	int id = 3; //joka sanastolla on yksil�llinen ID
	int oikein = 0;
	int taso = 0;
	int v1, v2;
	private List<String> kysymykset = Arrays.asList("Punainen", "Sininen", "Vihrea", "Oranssi", "Keltainen", "Pinkki",
			"Violetti", "Valkoinen", "Musta", "Ruskea", "Harmaa");
	private List<String> vastaukset = Arrays.asList("Red", "Blue", "Green", "Orange", "Yellow", "Pink", "Purple", "White",
			"Black", "Brown", "Grey");

	JLabel ohjeLabel = new JLabel("Valitse oikea kaannos englanniksi.");
	JLabel tarkistaO = new JLabel("OIKEIN");
	JLabel tarkistaV = new JLabel("VAARIN");
	JButton tarkista = new JButton("Tarkista");
	JButton jatka = new JButton("Jatka");

	public PeliVaritEN() {
		luoTaso();
        generator.setSeed(123456789);
		setSize(800, 1000);
		setLayout(null);
		Dimension koko = Toolkit.getDefaultToolkit().getScreenSize();
		setSize(koko.width, koko.height);
		setVisible(true);
	}

	// Luo peli tason
	private void luoTaso() {

		Color etusivuTausta = new Color(10, 123, 245);
		Font f1 = new Font(Font.SERIF, Font.BOLD, 60);
		Font f2 = new Font(Font.SERIF, Font.PLAIN, 20);

		JToggleButton ohje = new JToggleButton("Ohje");
		JLabel tasoluku = new JLabel("Taso " + (taso + 1) + "/" + kysymykset.size(), SwingConstants.CENTER);
		JLabel kysymys = new JLabel(kysymykset.get(taso), SwingConstants.CENTER);

		tasoluku.setFont(f1);
		ohjeLabel.setFont(f2);
		ohje.setBackground(etusivuTausta);

		JToggleButton vaihtoehto1 = new JToggleButton("");
		JToggleButton vaihtoehto2 = new JToggleButton("");
		JToggleButton vaihtoehto3 = new JToggleButton("");

		vaihtoehto1.setBounds(420, 300, 200, 75);
		vaihtoehto2.setBounds(670, 300, 200, 75);
		vaihtoehto3.setBounds(920, 300, 200, 75);

		ohje.setBounds(10, 100, 150, 75);

		kysymys.setBounds(700, 150, 140, 100);
		kysymys.setBorder(new LineBorder(Color.black, 1, false));

		tarkista.setBounds(670, 500, 200, 60);
		tarkista.setBackground(Color.YELLOW);

		jatka.setBounds(670, 500, 200, 60);
		jatka.setBackground(Color.YELLOW);

		JButton a = new JButton("Palaa etusivulle");
		a.setBounds(10, 10, 140, 40);
		a.setBackground(etusivuTausta);
		add(a);
		a.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				O4harjoitus etusivu = new O4harjoitus();
				dispose();
				etusivu.setVisible(true);
			}
		});

		arvoVaihtoehdot(vaihtoehto1, vaihtoehto2, vaihtoehto3);

		add(kysymys);
		add(vaihtoehto1);
		add(vaihtoehto2);
		add(vaihtoehto3);
		add(ohje);
		add(a, "North");
		add(tasoluku, "North");
		pack();
		setVisible(true);
		aloitaPeli(vaihtoehto1, vaihtoehto2, vaihtoehto3, ohje, tarkista);
	}

	// Aloittaa uuden pelin
	private void aloitaPeli(JToggleButton vaihtoehto1, JToggleButton vaihtoehto2, JToggleButton vaihtoehto3,
			JToggleButton ohje, JButton tarkista) {

		JLabel tasoluku = new JLabel("Taso " + (taso + 1) + "/" + kysymykset.size(), SwingConstants.CENTER);

		Color variV = new Color(255, 99, 99);
		Color variO = new Color(129, 242, 85);

		add(tasoluku);

		// Avautuu pelin ohjeet
		ohje.addItemListener(new ItemListener() {
			public void itemStateChanged(ItemEvent e) {

				if (ohje.isSelected() == true) {
					ohjeLabel.setBounds(10, 300, 400, 500);
					add(ohjeLabel);
					ohjeLabel.setVisible(true);
				} else {
					ohjeLabel.setVisible(false);
				}
			}
		});

		// Lisää tarkista- napin, kun vaihtoehto 1 on valittuna
		vaihtoehto1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {

				if (vaihtoehto1.isSelected() == true) {
					add(tarkista);
					tarkista.setVisible(true);
					add(jatka);
					jatka.setVisible(false);
					vaihtoehto2.setSelected(false);
					vaihtoehto3.setSelected(false);
				} else {
					tarkista.setVisible(false);
				}
			}
		});

		// Lisaa tarkista- napin, kun vaihtoehto 2 on valittuna
		vaihtoehto2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {

				if (vaihtoehto2.isSelected() == true) {
					tarkista.setVisible(true);
					add(tarkista);
					add(jatka);
					jatka.setVisible(false);
					vaihtoehto3.setSelected(false);
					vaihtoehto1.setSelected(false);
				} else {
					tarkista.setVisible(false);
				}
			}
		});

		// Lisää tarkista- napin, kun vaihtoehto 3 on valittuna
		vaihtoehto3.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {

				if (vaihtoehto3.isSelected() == true) {
					add(tarkista);
					add(jatka);
					jatka.setVisible(false);
					tarkista.setVisible(true);
					vaihtoehto2.setSelected(false);
					vaihtoehto1.setSelected(false);
				} else {
					tarkista.setVisible(false);
				}
			}
		});

		// Tarkistaa ja kertoo, onko käyttäjän valitsema vastaus oikea tai väärä
		tarkista.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String vastaus = vastaukset.get(taso);

				if (vaihtoehto1.isSelected() == true && vaihtoehto1.getText() == vastaus) {
					vaihtoehto1.setForeground(variO);
					tarkistaO.setBounds(50, 230, 200, 75);
					add(tarkistaO);
					oikein++;
				}
				if (vaihtoehto2.isSelected() == true && vaihtoehto2.getText() == vastaus) {
					vaihtoehto2.setForeground(variO);
					tarkistaO.setBounds(50, 230, 200, 75);
					add(tarkistaO);
					oikein++;
				}
				if (vaihtoehto3.isSelected() == true && vaihtoehto3.getText() == vastaus) {
					vaihtoehto3.setForeground(variO);
					tarkistaO.setBounds(50, 230, 200, 75);
					add(tarkistaO);
					oikein++;
				}
				if (vaihtoehto1.isSelected() == true && vaihtoehto1.getText() != vastaus) {
					vaihtoehto1.setForeground(variV);
					tarkistaV.setBounds(50, 230, 200, 75);
					add(tarkistaV);
				}
				if (vaihtoehto2.isSelected() == true && vaihtoehto2.getText() != vastaus) {
					vaihtoehto2.setForeground(variV);
					tarkistaV.setBounds(50, 230, 200, 75);
					add(tarkistaV);
				}
				if (vaihtoehto3.isSelected() == true && vaihtoehto3.getText() != vastaus) {
					vaihtoehto3.setForeground(variV);
					tarkistaV.setBounds(50, 230, 200, 75);
					add(tarkistaV);
				}
				tarkista.setVisible(false);
				jatka.setVisible(true);
			}
		});

		// Tarkistaa onko kaikki tasot käyty läpi. Jos on, peli loppuu. Jos ei,
		// siirtyy seuraavalle tasolle
		jatka.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				checkEnd();
				getContentPane().removeAll();
				uusiTaso();

				setSize(800, 1000);
				setLayout(null);
				Dimension koko = Toolkit.getDefaultToolkit().getScreenSize();
				setSize(koko.width, koko.height);
				setVisible(true);
			}
		});
	}

	private void uusiTaso() {

		Color etusivuTausta = new Color(10, 123, 245);
		Font f1 = new Font(Font.SERIF, Font.BOLD, 60);
		Font f2 = new Font(Font.SERIF, Font.PLAIN, 20);

		JToggleButton ohje = new JToggleButton("Ohje");
		JLabel tasoluku = new JLabel("Taso ", SwingConstants.CENTER);
		JLabel kysymys = new JLabel(kysymykset.get(taso), SwingConstants.CENTER);

		tasoluku.setFont(f1);
		ohjeLabel.setFont(f2);
		ohje.setBackground(etusivuTausta);

		JToggleButton vaihtoehto1 = new JToggleButton("");
		JToggleButton vaihtoehto2 = new JToggleButton("");
		JToggleButton vaihtoehto3 = new JToggleButton("");

		vaihtoehto1.setBounds(420, 300, 200, 75);
		vaihtoehto2.setBounds(670, 300, 200, 75);
		vaihtoehto3.setBounds(920, 300, 200, 75);

		ohje.setBounds(10, 100, 150, 75);

		kysymys.setBounds(700, 150, 140, 100);
		kysymys.setBorder(new LineBorder(Color.black, 1, false));

		tarkista.setBounds(670, 500, 200, 60);
		tarkista.setBackground(Color.YELLOW);

		jatka.setBounds(670, 500, 200, 60);
		jatka.setBackground(Color.YELLOW);

		JButton a = new JButton("Palaa etusivulle");
		a.setBounds(10, 10, 140, 40);
		a.setBackground(etusivuTausta);
		add(a);
		a.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				O4harjoitus etusivu = new O4harjoitus();
				dispose();
				etusivu.setVisible(true);
			}
		});

		arvoVaihtoehdot(vaihtoehto1, vaihtoehto2, vaihtoehto3);

		add(kysymys);
		add(vaihtoehto1);
		add(vaihtoehto2);
		add(vaihtoehto3);
		add(ohje);
		add(a);
		pack();
		setVisible(true);
		aloitaPeli(vaihtoehto1, vaihtoehto2, vaihtoehto3, ohje, tarkista);
	}

	// Vertaa tason numeroa kysymysten määrään. Kun kysymykset loppuvat,
	// loppuvat tasot
	private void checkEnd() {
		taso++;
		if (taso == kysymykset.size() - 1) {
			PeliLoppu peliLoppu = new PeliLoppu(oikein, kysymykset.size(), id);
		}
	}

	// Arpoo vastaukset nappeihin
	private void arvoVaihtoehdot(JToggleButton vaihtoehto1, JToggleButton vaihtoehto2, JToggleButton vaihtoehto3) {

		// arpoo v��r�t vastaukset kaikkien muiden tasojen oikeiden vastausten
		// joukosta
		v1 = taso;
		while (v1 == taso) {
			v1 = generator.nextInt(vastaukset.size());
		}

		v2 = generator.nextInt(vastaukset.size());
		while (v2 == taso || v2 == v1) {
			v2 = generator.nextInt(vastaukset.size());
		}

		// arpoo yhden kolmesta painikkeesta, johon oikea vastaus laitetaan
		int x = generator.nextInt(2);

		if(x == 0) {
			vaihtoehto1.setText(vastaukset.get(taso));
			vaihtoehto2.setText(vastaukset.get(v1));
			vaihtoehto3.setText(vastaukset.get(v2));
		}else if(x==1) {
			vaihtoehto2.setText(vastaukset.get(taso));
			vaihtoehto1.setText(vastaukset.get(v1));
			vaihtoehto3.setText(vastaukset.get(v2));
		}else if(x==2) {
			vaihtoehto3.setText(vastaukset.get(taso));
			vaihtoehto1.setText(vastaukset.get(v1));
			vaihtoehto2.setText(vastaukset.get(v2));
		}
	}

}