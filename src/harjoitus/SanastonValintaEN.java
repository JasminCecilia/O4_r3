package harjoitus;

import javax.swing.JFrame;
import javax.swing.ListSelectionModel;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

public class SanastonValintaEN extends JFrame implements ListSelectionListener {
	
	@Override
	public void valueChanged(ListSelectionEvent e) {
		ListSelectionModel listSelectionModel = (ListSelectionModel) e.getSource();
		O4harjoitus etusivu = new O4harjoitus();
		if (listSelectionModel.isSelectionEmpty() == false) {
			if (listSelectionModel.isSelectedIndex(0)) {
				dispose();
				AloitapeliVaritEN aloitaPeli = new AloitapeliVaritEN();
				etusivu.setVisible(false);
			} 
		}
	}
}
