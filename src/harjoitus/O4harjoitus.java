package harjoitus;

import java.awt.*;
import java.awt.event.ItemListener;
import java.awt.event.ItemEvent;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import javax.imageio.ImageIO;
import javax.swing.*;

//TODO Card Layout, jotta ei luo aina uutta ikkunaa sivua vaihtaessa?

public class O4harjoitus extends JFrame {

	JToggleButton englanti, ruotsi;
	JLabel otsake, ohje1, ohje2, a, b;
	JFrame f;

	public static void main(String[] args) {
		O4harjoitus sovellus = new O4harjoitus();
	}

	public O4harjoitus() {

		JPanel mainPanel = new JPanel();
		JPanel ohjePanel = new JPanel();
		JPanel valikkoPanel = new JPanel();
		JPanel lippuPanel = new JPanel();
		JPanel kieliPanel = new JPanel();
		JPanel nest = new JPanel();

		mainPanel.setLayout(new BorderLayout());
		ohjePanel.setLayout(new BoxLayout(ohjePanel, BoxLayout.Y_AXIS));
		kieliPanel.setLayout(new BoxLayout(kieliPanel, BoxLayout.X_AXIS));
		lippuPanel.setLayout(new BoxLayout(lippuPanel, BoxLayout.X_AXIS));
		valikkoPanel.setLayout(new GridBagLayout());
		nest.setLayout(new BoxLayout(nest, BoxLayout.Y_AXIS));

		ohjePanel.setAlignmentX(CENTER_ALIGNMENT);
		
		GridBagConstraints c = new GridBagConstraints();

		JLabel otsake = new JLabel("Kielipeli", SwingConstants.CENTER);
		Font f1 = new Font(Font.SERIF, Font.BOLD, 60);
		otsake.setFont(f1);

		JLabel ohje = new JLabel("1. Valitse opeteltava sana klikkaamalla lippua.", SwingConstants.CENTER);
		JLabel ohje2 = new JLabel("2. Valitse opeteltava sanasto avautuneesta valikosta.", SwingConstants.CENTER);
		Font f2 = new Font(Font.SERIF, Font.PLAIN, 20);
		ohje.setFont(f2);
		ohje2.setFont(f2);
		
		otsake.setAlignmentX(CENTER_ALIGNMENT);
		ohje.setAlignmentX(CENTER_ALIGNMENT);
		ohje2.setAlignmentX(CENTER_ALIGNMENT);

		ohjePanel.add(otsake);
		ohjePanel.add(ohje);
		ohjePanel.add(ohje2);

		Font f3 = new Font(Font.SERIF, Font.BOLD, 20);

		JLabel a = new JLabel("Englanti                           ", SwingConstants.CENTER);
		a.setFont(f3);
		JToggleButton englanti = new JToggleButton();

		JLabel b = new JLabel("                           Ruotsi", SwingConstants.CENTER);
		b.setFont(f3);
		JToggleButton ruotsi = new JToggleButton();
        
		File img1 = new File("resources/800px-Flag_of_Sweden_PIENI2.png");
		File img2 = new File("resources/Flag_of_the_United_Kingdom_(3-5).svg_PIENI.png");
		
		try {
			ruotsi.setIcon(new ImageIcon(ImageIO.read(img1)));
			ruotsi.setMargin(new Insets(0, 0, 0, 0));
			ruotsi.setBorder(null);
			ruotsi.setBorderPainted(false); ruotsi.setContentAreaFilled(false);
			englanti.setIcon(new ImageIcon(ImageIO.read(img2)));
			englanti.setMargin(new Insets(0, 0, 0, 0));
			englanti.setBorder(null);
			englanti.setBorderPainted(false); englanti.setContentAreaFilled(false);
		} catch (IOException e1) {
			e1.printStackTrace();
		}
		JLabel empty = new JLabel("                                          ");

		kieliPanel.add(a); // eng kielen nimi
		kieliPanel.add(b); // sv kielen nimi

		lippuPanel.add(englanti); // eng lippu
		lippuPanel.add(empty);
		lippuPanel.add(ruotsi); // sv lippu
		
		List<String> lista1 = new ArrayList<>(2);
		lista1.add("Varit");
		englanti.addItemListener(new ItemListener() {
			public void itemStateChanged(ItemEvent e) {
				c.fill = GridBagConstraints.RELATIVE;
				c.weighty = 0.1;
				c.weightx = 0.5;
				c.insets = new Insets(0, 0, 100, 100);
				c.gridx = 2;
				c.gridy = 4;

				final JList<String> sanastotEN = new JList<String>(lista1.toArray(new String[lista1.size()]));
				JScrollPane valikkoEN = new JScrollPane(sanastotEN);
				sanastotEN.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
				sanastotEN.setLayoutOrientation(JList.VERTICAL);
				sanastotEN.setVisibleRowCount(3);
				valikkoEN.setPreferredSize(new Dimension(100, 150));
				ohjePanel.remove(empty);
				valikkoPanel.add(sanastotEN, c);

				if (englanti.isSelected() == false) {
					valikkoPanel.setVisible(true);
				} else {
					valikkoPanel.setVisible(false);
				}

				ListSelectionModel listSelectionModel = sanastotEN.getSelectionModel();
				listSelectionModel.addListSelectionListener(new SanastonValintaEN());
				
			}
		});
		
		List<String> lista2 = new ArrayList<>(2);
		lista2.add("Elaimet");
		lista2.add("Varit");
		ruotsi.addItemListener(new ItemListener() {
			public void itemStateChanged(ItemEvent e) {
				c.fill = GridBagConstraints.RELATIVE;
				c.weighty = 0.1;
				c.weightx = 0.5;
				c.insets = new Insets(0, 0, 100, 100);
				c.gridx = 4;
				c.gridy = 4;

				final JList<String> sanastotSV = new JList<String>(lista2.toArray(new String[lista2.size()]));
				JScrollPane valikkoSV = new JScrollPane(sanastotSV);
				sanastotSV.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
				sanastotSV.setLayoutOrientation(JList.VERTICAL);
				sanastotSV.setVisibleRowCount(3);
				valikkoSV.setPreferredSize(new Dimension(100, 150));
				ohjePanel.remove(empty);
				valikkoPanel.add(sanastotSV, c);

				if (ruotsi.isSelected() == false) {
					valikkoPanel.setVisible(true);
				} else {
					valikkoPanel.setVisible(false);
				}

				ListSelectionModel listSelectionModel = sanastotSV.getSelectionModel();
				listSelectionModel.addListSelectionListener(new SanastonValintaSV());
				
			}
		});
	
		nest.add(kieliPanel);
		nest.add(lippuPanel);
		mainPanel.add(ohjePanel, BorderLayout.PAGE_START);
		mainPanel.add(nest, BorderLayout.CENTER);
		mainPanel.add(valikkoPanel, BorderLayout.SOUTH);
		this.getContentPane().add(mainPanel);
		pack();
		Dimension koko = Toolkit.getDefaultToolkit().getScreenSize();
		setSize(koko.width, koko.height);
		setVisible(true);
	}
}
