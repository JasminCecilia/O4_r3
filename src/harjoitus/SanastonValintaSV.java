package harjoitus;

import javax.swing.JFrame;
import javax.swing.ListSelectionModel;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

public class SanastonValintaSV extends JFrame implements ListSelectionListener {
	
	@Override
	public void valueChanged(ListSelectionEvent e) {
		ListSelectionModel listSelectionModel = (ListSelectionModel) e.getSource();
		O4harjoitus etusivu = new O4harjoitus();
		if (listSelectionModel.isSelectionEmpty() == false) {
			dispose();
			if (listSelectionModel.isSelectedIndex(0)) {
				AloitapeliElaimetSV aloitaPeli = new AloitapeliElaimetSV();
			} else if (listSelectionModel.isSelectedIndex(1)) {
				Aloitapeli2 aloitaPeli = new Aloitapeli2();
			}
			etusivu.setVisible(false);
		}
	}
}
