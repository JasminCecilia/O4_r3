package harjoitus;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import java.awt.*;

public class AloitapeliVaritEN extends JFrame {

	public AloitapeliVaritEN() {
		
		Color etusivuTausta = new Color(10, 123, 245);
		Font f1 = new Font(Font.SERIF, Font.BOLD, 60);
		Font f2 = new Font(Font.SERIF, Font.PLAIN, 20);

		JLabel j = new JLabel("Varit / Colours");
		j.setBounds(600, 70, 800, 100);
		j.setFont(f1);
		add(j);

		JLabel k = new JLabel("1. Pelissa valitse kolmesta kaannosvaihtoehdosta oikea");
		k.setBounds(560, 150, 500, 100);
		add(k);
		k.setFont(f2);

		JLabel l = new JLabel("2. Kun olet valmis pelaamaan, valitse 'Aloita peli'");
		l.setBounds(560, 200, 500, 100);
		add(l);
		l.setFont(f2);

		JButton a = new JButton("Palaa etusivulle");
		a.setBounds(10, 10, 140, 40);
		a.setBackground(etusivuTausta);
		add(a);

		a.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				O4harjoitus etusivu = new O4harjoitus();
				dispose();
				etusivu.setVisible(true);
			}
		});

		JButton b = new JButton("Aloita peli");
		b.setBounds(720, 300, 100, 40);
		add(b);

		b.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				PeliVaritEN peli = new PeliVaritEN();
				dispose();
				peli.setVisible(true);

			}
		});

		setSize(800, 1000);
		setLayout(null);
		Dimension koko = Toolkit.getDefaultToolkit().getScreenSize();
		setSize(koko.width, koko.height);
		setVisible(true);
	}

}